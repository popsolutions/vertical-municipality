from odoo import api, fields, models


class PropertyLand(models.Model):
    _inherit = 'property.land'

    wc_connection_date = fields.Date()
    wc_charging_date = fields.Date()
    wc_sewage_connection_date = fields.Date()
    wc_sewage_charging_date = fields.Date()
    wc_route_id = fields.Many2one('property.water_consumption.route')
    wc_param_id = fields.Many2one('property.water_consumption.comp_parameter')
    wc_meter_code = fields.Char()
    wc_economy_qty = fields.Integer()
    wc_unit_qty = fields.Integer()

    wc_count = fields.Integer(compute='_compute_wc_count', string='Water Consumption Count')

    def _compute_wc_count(self):
        wc_obj = self.env['property.water_consumption'].sudo()
        for rec in self:
            rec.wc_count = wc_obj.search_count([('land_id', '=', rec.id)])