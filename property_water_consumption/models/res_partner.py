from odoo import api, fields, models


class ResPartner(models.Model):
    _inherit = 'res.partner'

    wc_reader = fields.Boolean('Is a Reader')
